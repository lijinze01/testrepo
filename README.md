# Summary #
1. Test tool - Selenium Web Driver
2. IDE - Eclipse
3. Programming Language - Java
4. Framework - JBehave BDD framework
5. Page objects pattern - Yes, page objects are saved in the page packages
6. JDK version: "1.7.0_17"

# How to run test #
Please run below story runner
src\main\auto\myConnect\regression\storyrunner\Chrome_ConfluenceUserCanCreateANewPage.java
src\main\auto\myConnect\regression\storyrunner\Chrome_ConfluenceUserCanSetRestrictionsOnExistingPage.java

# Logging system #
1. For each run, a text log file will be created in "logs" folder
2. Below log generator will also create a HTML version of summary report
src\main\auto\myConnect\util\LogGenerator.java

# Note #
These are just 2 simple tests, so they are well self-sufficient, no pre-requesit needed, and the script will clean the environment after each run.