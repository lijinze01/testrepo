Narrative:
Test that a user can set restrictions on existing page
As a user
Test that a user can set restrictions on existing page

Story: Test that a user can set restrictions on existing page
			 
Scenario: Test that a user can set restrictions on existing page

GivenStories: /main/auto/myConnect/common/story/login.story

Given Create a test blank page
Then Make sure user is able to click the restrictions option from the action menu list
Then Make sure the update page restrictions dialog is showing up
Then Close the restrictions dialog
Then Delete the test page
Then Logout
Examples:
|url									|username			    |password|
|https://atlassianqa.atlassian.net/login|lijinze01@gmail.com	|passw0rd|