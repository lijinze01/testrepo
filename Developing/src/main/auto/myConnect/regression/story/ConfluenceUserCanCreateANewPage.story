Narrative:
Test that a user can create a new page
As a user
Can create a new page

Story: Test that a user can create a new page
			 
Scenario: Test that a user can create a new page

GivenStories: /main/auto/myConnect/common/story/login.story

Given Make sure the quick create page button is available
When Click the quick create page button
Then A new blank page is displayed
Given Make sure the create page button is available
When Click the create page button
Then The create page pop-up with <pageTemplates> is showing up
Then Close create page dialog
Then Logout
Examples:
|url									|username			    |password|pageTemplates|
|https://atlassianqa.atlassian.net/login|lijinze01@gmail.com	|passw0rd|JIRA report  |