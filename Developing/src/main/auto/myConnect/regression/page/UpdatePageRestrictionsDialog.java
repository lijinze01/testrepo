package main.auto.myConnect.regression.page;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import main.auto.myConnect.testframework.SuperWebDriverPage;

public class UpdatePageRestrictionsDialog extends SuperWebDriverPage {

	public UpdatePageRestrictionsDialog(WebDriverProvider driverProvider) {
		super(driverProvider);
	}

	// Update page restrictions dialog
	public WebElement updatePageRestrictionsDialog() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("section[id='update-page-restrictions-dialog']"),3);
	}


	// Cancel button
	public WebElement cancelBtn() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("button[id='page-restrictions-dialog-close-button']"),3);
	}
	

}
