package main.auto.myConnect.regression.page;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import main.auto.myConnect.testframework.SuperWebDriverPage;

public class BlankPage extends SuperWebDriverPage {

	public BlankPage(WebDriverProvider driverProvider) {
		super(driverProvider);
	}

	// Blank page - Content title
	public WebElement blankPageContentTitle() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("input[id='content-title'][name='title'][placeholder='Page title']"),3);
	}

	// Save button
	public WebElement saveBtn() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("button[id='rte-button-publish']"),3);
	}

	// Page title link
	public WebElement pageTitleLink(String linkText) throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.linkText(linkText),3);
	}

	// Action menu link
	public WebElement actionMenuLink() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("a[id='action-menu-link']"),3);
	}

	// Restrictions option
	public WebElement restrictionsOption() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("a[id='action-page-permissions-link']"),3);
	}

	// Delete option
	public WebElement deleteOption() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("a[id='action-remove-content-link']"),3);
	}
	
	// Ok button
	public WebElement oKBtn() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("input[id='confirm']"),3);
	}

	


}
