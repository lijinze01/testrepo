package main.auto.myConnect.regression.page;

import java.util.ArrayList;
import java.util.List;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import main.auto.myConnect.testframework.SuperWebDriverPage;

public class CreatePageDialog extends SuperWebDriverPage {

	public CreatePageDialog(WebDriverProvider driverProvider) {
		super(driverProvider);
	}

	// Close dialog link
	public WebElement closeDialogLink() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.linkText("Close"));
	}
	
	// Template names
	public String[] templateNames() throws Exception{
		List<String> listOfNames = new ArrayList<String>();
		waitForPageLoad();
		List<WebElement> listOfTemplateNames = waitForPresenceOfElements(By.cssSelector("div[class='template-name']"));
		for(WebElement templateName:listOfTemplateNames){
			listOfNames.add(templateName.getText());
		}
		return listOfNames.toArray(new String[listOfNames.size()]);
	}

}
