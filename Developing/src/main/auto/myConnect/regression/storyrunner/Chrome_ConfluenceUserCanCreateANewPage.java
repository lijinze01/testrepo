 package main.auto.myConnect.regression.storyrunner;

import static java.util.Arrays.asList;
import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;

import java.util.List;

import org.jbehave.core.io.StoryFinder;

import main.auto.myConnect.common.step.LoginToAtlassian;
import main.auto.myConnect.common.step.TestPreparation;
import main.auto.myConnect.regression.step.ConfluenceUserCanCreateANewPage;
import main.auto.myConnect.testframework.AbstractStoryRunner;



public class Chrome_ConfluenceUserCanCreateANewPage extends AbstractStoryRunner
{

	
	protected Object[] getSteps() 
	{
		// Set Chrome WebDriver
		System.setProperty("browser", "Chrome");
		// Set log name
		System.setProperty("logFilename", "Chrome_ConfluenceUserCanCreateANewPage");
		
		return new Object[] { 
				new TestPreparation(driverProvider), 
				new LoginToAtlassian(factory),
				new ConfluenceUserCanCreateANewPage(factory),
		};
	}

	@Override
	protected List<String> storyPaths() {
		String codeLocation = codeLocationFromClass(this.getClass()).getFile();

		String story = System.getProperty("story", "ConfluenceUserCanCreateANewPage");

		return new StoryFinder().findPaths(codeLocation,
				asList(buildStories("regression", story)), asList(""));
	}
}