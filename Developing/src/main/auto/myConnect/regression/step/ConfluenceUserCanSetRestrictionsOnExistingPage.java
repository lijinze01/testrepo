package main.auto.myConnect.regression.step;

import java.io.IOException;
import java.sql.Timestamp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;

import main.auto.myConnect.common.page.AtlassianLogoutPage;
import main.auto.myConnect.common.page.ConfluenceHomePage;
import main.auto.myConnect.regression.page.BlankPage;
import main.auto.myConnect.regression.page.UpdatePageRestrictionsDialog;
import main.auto.myConnect.testframework.PageFactory;
import main.auto.myConnect.testframework.ScriptSuperClass;

public class ConfluenceUserCanSetRestrictionsOnExistingPage extends ScriptSuperClass {
	private ConfluenceHomePage confluenceHomePage;
	private BlankPage blankPage;
	private UpdatePageRestrictionsDialog updatePageRestrictionsDialog;
	private AtlassianLogoutPage logoutPage;
	private String pageTitle = "";
	
	final Logger logger = LogManager.getLogger(ConfluenceUserCanSetRestrictionsOnExistingPage.class);
	boolean statusOfPreviousStep = true;

	public ConfluenceUserCanSetRestrictionsOnExistingPage(PageFactory factory) 
	{
		this.confluenceHomePage = factory.getPage(ConfluenceHomePage.class);
		this.blankPage = factory.getPage(BlankPage.class);
		this.updatePageRestrictionsDialog = factory.getPage(UpdatePageRestrictionsDialog.class);
		this.logoutPage = factory.getPage(AtlassianLogoutPage.class);
	}
	
	
	@Given("Create a test blank page")
	public void createTestPage() throws IOException{
		try{
			// Define page name = static string + time stamp
			setPageTitle("TEST" + "["+new Timestamp(new java.util.Date().getTime())+"]");
			
			// Click the quick create page link
			confluenceHomePage.quickCreatePageBtn().click();
			// Enter page name
			blankPage.blankPageContentTitle().sendKeys(getPageTitle());
			// Save page
			blankPage.saveBtn().click();
			// Make sure the page is successfully saved - Should be able to find the page title as a link
			blankPage.pageTitleLink(getPageTitle());
			
			logger.info("PASS - Create a test blank page");
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL - Create a test blank page");
			logger.error("Error Description", e);
			confluenceHomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}

	@Then("Make sure user is able to click the restrictions option from the action menu list")
	public void clickRestrictionsOption() throws IOException{
		try{
			checkStatusOfPreviousStep(statusOfPreviousStep);
			
			// Click the action menu link
			blankPage.actionMenuLink().click();
			sleep(1);
			// Click the restrictions option
			blankPage.restrictionsOption().click();
			
			logger.info("PASS - Make sure user is able to click the restrictions option from the action menu list");
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL - Make sure user is able to click the restrictions option from the action menu list");
			logger.error("Error Description", e);
			confluenceHomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}
	
	@Then("Make sure the update page restrictions dialog is showing up")
	public void checkRestrictionsDialog() throws IOException{
		try{
			checkStatusOfPreviousStep(statusOfPreviousStep);
			// Find the update page restrictions dialog
			updatePageRestrictionsDialog.updatePageRestrictionsDialog();
			
			logger.info("PASS - Make sure the update page restrictions dialog is showing up");
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL - Make sure the update page restrictions dialog is showing up");
			logger.error("Error Description", e);
			confluenceHomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}
	
	@Then("Close the restrictions dialog")
	public void closeRestrictionsDialog() throws Exception{
		try{
			checkStatusOfPreviousStep(statusOfPreviousStep);
			// Click cancel button on the update page restrictions dialog
			updatePageRestrictionsDialog.cancelBtn().click();
			
			logger.info("PASS - Close the restrictions dialog");
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL - Close the restrictions dialog");
			logger.error("Error Description", e);
			confluenceHomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}
	
	@Then("Delete the test page")
	public void deleteTheTestPage() throws Exception{
		try{
			checkStatusOfPreviousStep(statusOfPreviousStep);
			// Click the action menu link
			blankPage.tryToClick(blankPage.actionMenuLink());
//			blankPage.actionMenuLink().click();
//			sleep(1);
			// Click the delete option
			blankPage.deleteOption().click();
			sleep(2);
			// Click confirm button
			blankPage.oKBtn().click();
			
			logger.info("PASS - Delete the test page");
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL - Delete the test page");
			logger.error("Error Description", e);
			confluenceHomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}

	@Then("Logout")
	public void logout() throws Exception{
		try{
			checkStatusOfPreviousStep(statusOfPreviousStep);
			confluenceHomePage.userMenuLink().click();
			sleep(1);
			confluenceHomePage.logoutLink().click();
			logoutPage.confirmLogout();
			logoutPage.logoutBtn().click();
			logoutPage.logoutMessage();
			logger.info("PASS - Logout");
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL - Logout");
			logger.error("Error Description", e);
			logoutPage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	
}
