package main.auto.myConnect.regression.step;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import main.auto.myConnect.common.page.AtlassianLogoutPage;
import main.auto.myConnect.common.page.ConfluenceHomePage;
import main.auto.myConnect.regression.page.BlankPage;
import main.auto.myConnect.regression.page.CreatePageDialog;
import main.auto.myConnect.testframework.PageFactory;
import main.auto.myConnect.testframework.ScriptSuperClass;

public class ConfluenceUserCanCreateANewPage extends ScriptSuperClass {
	private ConfluenceHomePage confluenceHomePage;
	private BlankPage blankPage;
	private CreatePageDialog createPageDialog;
	private AtlassianLogoutPage logoutPage;

	final Logger logger = LogManager.getLogger(ConfluenceUserCanCreateANewPage.class);
	boolean statusOfPreviousStep = true;

	public ConfluenceUserCanCreateANewPage(PageFactory factory) 
	{
		this.confluenceHomePage = factory.getPage(ConfluenceHomePage.class);
		this.blankPage = factory.getPage(BlankPage.class);
		this.createPageDialog = factory.getPage(CreatePageDialog.class);
		this.logoutPage = factory.getPage(AtlassianLogoutPage.class);
	}

	@Given("Make sure the quick create page button is available")
	public void makeSureQuickCreatePageButtonIsAvailable() throws IOException{
		try{
			confluenceHomePage.quickCreatePageBtn();
			logger.info("PASS - Make sure the quick create page button is available");
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL - Make sure the quick create page button is available");
			logger.error("Error Description", e);
			confluenceHomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}

	@When("Click the quick create page button")
	public void clickTheQuickCreatePageButton() throws IOException{
		try{
			checkStatusOfPreviousStep(statusOfPreviousStep);
			confluenceHomePage.quickCreatePageBtn().click();
			logger.info("PASS - Click the quick create page button");
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL - Click the quick create page button");
			logger.error("Error Description", e);
			confluenceHomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}

	@Then("A new blank page is displayed")
	public void checkNewBlankPage() throws IOException{
		try{
			checkStatusOfPreviousStep(statusOfPreviousStep);
			blankPage.blankPageContentTitle();
			logger.info("PASS - A new blank page is displayed");
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL - A new blank page is displayed");
			logger.error("Error Description", e);
			confluenceHomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}
	
	@Given("Make sure the create page button is available")
	public void makeSureTheCreatePageButtonIsAvailable() throws IOException{
		try{
			checkStatusOfPreviousStep(statusOfPreviousStep);
			confluenceHomePage.createPageBtn();
			logger.info("PASS - Make sure the create page button is available");
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL -  - Make sure the quick create page button is available");
			logger.error("Error Description", e);
			confluenceHomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}

	@When("Click the create page button")
	public void clickTheCreatePageButton() throws IOException{
		try{
			checkStatusOfPreviousStep(statusOfPreviousStep);
			confluenceHomePage.createPageBtn().click();
			logger.info("PASS - Click the create page button");
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL - Click the create page button");
			logger.error("Error Description", e);
			confluenceHomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}
	
	@Then("The create page pop-up with <pageTemplates> is showing up")
	public void checkPageTemplates(@Named("pageTemplates") String pageTemplates) throws IOException{
		try{
			checkStatusOfPreviousStep(statusOfPreviousStep);
			boolean templateFound = false;
			String[] actualNames = createPageDialog.templateNames();
			for(String name:actualNames){
				if(name.equalsIgnoreCase(pageTemplates)){
					templateFound = true;
					break;
				}
			}
			
			if(templateFound){
				logger.info("PASS - The create page pop-up with <pageTemplates> [{}] is showing up", pageTemplates);
			}else{
				throw new Exception("User is not able to see the page template ["+pageTemplates+"] from create page dialog!");
			}
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL - The create page pop-up with <pageTemplates> [{}] is showing up", pageTemplates);
			logger.error("Error Description", e);
			confluenceHomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}
	
	@Then("Close create page dialog")
	public void closeCreatePageDialog() throws Exception{
		try{
			checkStatusOfPreviousStep(statusOfPreviousStep);
			createPageDialog.closeDialogLink().click();
			sleep(1);
			logger.info("PASS - Close create page dialog");
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL - Close create page dialog");
			logger.error("Error Description", e);
			confluenceHomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}

	@Then("Logout")
	public void logout() throws Exception{
		try{
			checkStatusOfPreviousStep(statusOfPreviousStep);
			confluenceHomePage.userMenuLink().click();
			sleep(1);
			confluenceHomePage.logoutLink().click();
			logoutPage.confirmLogout();
			logoutPage.logoutBtn().click();
			logoutPage.logoutMessage();
			logger.info("PASS - Logout");
		}catch (Exception e){
			statusOfPreviousStep = false;
			logger.error("FAIL - Logout");
			logger.error("Error Description", e);
			logoutPage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
		}
	}
}
