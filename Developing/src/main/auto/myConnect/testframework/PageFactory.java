package main.auto.myConnect.testframework;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import org.jbehave.web.selenium.WebDriverProvider;

public class PageFactory {

	WebDriverProvider driverProvider;
	Map<Class<?>, Object> pageBucket;
	
	public PageFactory(WebDriverProvider driverProvider) {
		pageBucket = new HashMap<Class<?>, Object>();
		this.driverProvider = driverProvider;
	}
	
	private <V> V createPageObject(Class<V> pageType)
	{
		Constructor<V> constructor;
		try 
		{
			constructor = pageType.getDeclaredConstructor(WebDriverProvider.class);
			V instance = constructor.newInstance(this.driverProvider);
			return instance;
		} 
		catch (Exception e) 
		{
			StringBuilder builder = new StringBuilder(
					"no implementing constructor found.").append("\n")
					.append("Page objects should implement constructor taking WebDriverProvider");
			throw new RuntimeException(builder.toString());
		}
	}
	
	@SuppressWarnings("unchecked")
	public <V> V getPage(Class<V> pageType)
	{
		if (pageBucket.containsKey(pageType)) 
		{
			return (V) pageBucket.get(pageType);
		}

		V instance = createPageObject(pageType);
		pageBucket.put(pageType, instance);
		return instance;
	}
	

}
