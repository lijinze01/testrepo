package main.auto.myConnect.testframework;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jbehave.web.selenium.WebDriverPage;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

public class SuperWebDriverPage extends WebDriverPage{

	final Logger logger = LogManager.getLogger(SuperWebDriverPage.class);

	public SuperWebDriverPage(WebDriverProvider driverProvider) {
		super(driverProvider);
	}

	public static final int TIMEOUT = 20;

	public void setImplicitWait(int iTimeInSec){
		getDriverProvider().get().manage().timeouts().implicitlyWait(iTimeInSec, TimeUnit.SECONDS);
	}

	public WebElement waitForPresenceOfElement(By by) throws Exception
	{
		WebDriverWait wait = new WebDriverWait(getDriverProvider().get(), TIMEOUT);
		WebElement testObject;
		try {
			testObject = wait.until(ExpectedConditions.presenceOfElementLocated(by));
		} catch (Exception e) {
			throw new Exception("Objects not found - " + by.toString());
		}
		return testObject;
	}
	
	public List<WebElement> waitForPresenceOfElements(By by) throws Exception
	{
		WebDriverWait wait = new WebDriverWait(getDriverProvider().get(), TIMEOUT);
		List<WebElement> testObjects;
		try {
			testObjects = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
		} catch (Exception e) {
			throw new Exception("Objects not found - " + by.toString());
		}
		return testObjects;
	}

	public WebElement waitForPresenceOfElement(By by, int howManyTimesTryToFind) throws Exception
	{
		boolean found = false;
		int ctr = 0;
		WebDriverWait wait = new WebDriverWait(getDriverProvider().get(), 5);
		WebElement testObject = null;
		while(found == false){
			try {
				testObject = wait.until(ExpectedConditions.presenceOfElementLocated(by));
				found = true;
			} catch (Exception e) {
				sleep(1);
				ctr ++;
				if(ctr > howManyTimesTryToFind){
					throw new Exception("Objects not found - " + by.toString());
				}
			}
		}
		return testObject;
	}

	/**
	 * Find test object by CSS Selector
	 * 
	 * @param sTageName e.g. "input"
	 * @param sPNamesAndValues e.g. "name:line_opr" "" ""
	 * @return
	 * @throws Exception 
	 */
	public WebElement waitForPresenceOfElementByCssSelector(String sTageName, String... saPNamesAndValues) throws Exception{

		StringBuilder  sb = new StringBuilder();
		String sCssSelectorCriteria = "";

		if(saPNamesAndValues.length == 0){
			throw new Exception("Cannot find test object without property name and value!");
		}

		//"input[name=''][id=''][type='']"
		for(String pNameAndValue:saPNamesAndValues){
			String singleNameAndValueFrame = "[propertyName='propertyValue']";
			String sPropertyName = "";
			String sPropertyValue = "";

			try{
				sPropertyName = pNameAndValue.split(":")[0].toString();
				sPropertyValue = pNameAndValue.split(":")[1].toString();
			}catch (Exception e){
				throw new Exception("saPNamesAndValues - Invalid formate!");
			}

			sb.append(singleNameAndValueFrame.replace("propertyName", sPropertyName).replace("propertyValue", sPropertyValue));
		}

		sCssSelectorCriteria = sTageName + sb.toString();

		WebDriverWait wait = new WebDriverWait(getDriverProvider().get(), TIMEOUT);
		WebElement testObject;
		try {
			testObject = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(sCssSelectorCriteria)));
		} catch (Exception e) {
			throw new Exception("Objects not found - By CssSelector " + sCssSelectorCriteria);
		}


		return testObject;
	}

	public void load(String sURL){
		WebDriver driver = getDriverProvider().get();
		driver.get(sURL);
	}

	public void setPageLoadTimeout(int iTimeoutInSec){
		getDriverProvider().get().manage().timeouts().pageLoadTimeout(iTimeoutInSec, TimeUnit.SECONDS);
	}

	public void waitForPageLoad() {
		Wait<WebDriver>
		wait = new WebDriverWait(getDriverProvider().get(), 120);
		wait.until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver driver) {
				boolean ready = false;
				try{
					ready = String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState")).equals("complete");
				}catch(Exception e){}
				//System.out.println("Current Window State: " + String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState")));
				return ready;
			}
		});
	}
	

	public void sleep(int seconds) 
	{
		for (int i = 0; i < seconds; i++) 
		{
			try 
			{
				Thread.sleep(1000);
			} 
			catch (InterruptedException e) 
			{
				Thread.interrupted();
			}
		}
	}

	public void sleep(double seconds) 
	{
		try 
		{
			Thread.sleep((long) (seconds*1000));
		} 
		catch (InterruptedException e) 
		{
			Thread.interrupted();
		}
	}

	public boolean isNotNullOrBlank(String sVar)
	{
		return !isNullOrBlank(sVar);
	}//isNotNullOrBlank

	public boolean isNullOrBlank(String sVar)
	{
		boolean result = false;
		if(sVar == null || sVar.isEmpty())
		{
			result = true;
		}
		return result;
	}//isNullOrBlank

	public void screenshot(String sPorF, String sPath, String sFilename) throws IOException{
		String date = new java.text.SimpleDateFormat("dd-MM-yyyy HH-mm-ss").format(new java.util.Date ());
		File scrFile = ((TakesScreenshot)getDriverProvider().get()).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		FileUtils.copyFile(scrFile, new File(sPath + date + "-" + sFilename + ".png")); 
		logger.info(sPorF + " - Screenshot is saved at {}.", sPath + date + "-" + sFilename + ".png");
	}

	public void tryToClick(WebElement testObject) throws Exception{
		int ctr = 0;
		boolean click = false;
		while(click == false){
			try{
				testObject.click();
				click = true;
			}catch(Exception e){
				ctr ++;
				sleep(1);
			}
			if(ctr > 60){
				throw new Exception("Cannot Click Test Object: " + testObject);
			}
		}
	}
	
	
}
