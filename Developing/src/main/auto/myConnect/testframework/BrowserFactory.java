package main.auto.myConnect.testframework;

import java.io.File;

public class BrowserFactory 
{
	private static final String BROWSER_PROP_KEY = "browser";
	private static final String workingDir = System.getProperty("user.dir");
	private static final String FS = File.separator;
	private static final String sIEDriver_32 = "IEDriverServer_32.exe";
	private static final String sIEDriver_64 = "IEDriverServer_64.exe";
	private static final String sChromeDriver = "chromedriver.exe";

	/**
	 * creates the browser driver specified in the system property "browser"
	 * if no property is set then a IE browser driver is created.
	 * The allow properties are IE & Chrome
	 * e.g to run with chrome, pass in the option -Dbrowser=chrome at runtime
	 * @return WebDriver
	 */
	public static void setupBrowser() 
	{
		String sBrowser;

		if(System.getProperty(BROWSER_PROP_KEY)==null){
			
			// Set default browser is IE 32bit
			sBrowser = Browsers.browserForName("IE_32");
			System.setProperty("browser", "IE");
		}else{
			sBrowser = Browsers.browserForName(System.getProperty(BROWSER_PROP_KEY));
		}
		
		if(sBrowser.equalsIgnoreCase("IE_32")){
			setIEDriver_32();
			System.setProperty("browser", "IE");
		}
		else if(sBrowser.equalsIgnoreCase("IE_64")){
			setIEDriver_64();
			System.setProperty("browser", "IE");
		}
		else if(sBrowser.equalsIgnoreCase("Chrome")){
			setChromeDriver();
		}
		else if(sBrowser.equalsIgnoreCase("Safari")){// For future use
			setSafariDriver();
		}
		else if(sBrowser.equalsIgnoreCase("Firefox")){// For future use
			setFirefoxDriver();
		}
		else{
			setIEDriver_32(); // Default
		}
	}

	private static void setIEDriver_32() 
	{
		// Set IE driver server - 32bit
		System.setProperty("webdriver.ie.driver", workingDir  + FS + "driverServers" + FS + sIEDriver_32);
		
	}
	
	private static void setIEDriver_64()
	{
		// Set IE driver server - 64bit
		System.setProperty("webdriver.ie.driver", workingDir  + FS + "driverServers" + FS + sIEDriver_64);
	}

	private static void setChromeDriver() 
	{
		// Set Chrome driver server
		System.setProperty("webdriver.chrome.driver", workingDir  + FS + "driverServers" + FS + sChromeDriver);
	}

	private static void setFirefoxDriver() 
	{
		// Set Firefox driver - Do not need set system property
	}
	
	private static void setSafariDriver() 
	{
		// Set Safari driver - Do not need set system property
	}
}
