package main.auto.myConnect.testframework;

public enum Browsers 
{
	INTENETEXPLORER_32("IE_32"),
	INTENETEXPLORER_64("IE_64"),
	CHROME("Chrome"),
	FIREFOX("Firefox"), 
	SAFARI("Safari"); // For future use

	private final String sBrowserName;
	
	Browsers(String sBrowserName)
	{
		this.sBrowserName = sBrowserName;
	}
	
	
	public static String browserForName(String browser) throws IllegalArgumentException
	{
        String targetBrowser = null;
		for(Browsers b: values())
        {
    		if(b.sBrowserName.equalsIgnoreCase(browser))
    		{
    			targetBrowser = b.sBrowserName;
    			break;
    		}
        }
		
		if(targetBrowser == null)
		{
			throw browserNotFound(browser);
		}
		else
		{
			return targetBrowser;
		}
    }

    private static IllegalArgumentException browserNotFound(String invalidBrowserName) 
    {
        return new IllegalArgumentException(("Invalid browser [" + invalidBrowserName + "]"));
    }
}
