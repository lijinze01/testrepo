package main.auto.myConnect.testframework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.WebDriver;

import main.auto.myConnect.util.Messages;

public class ScriptSuperClass {

	private final String TASKLIST = "tasklist /v"; 
	private final String KILL = "taskkill /F /PID "; 

	

	public boolean isNotNullOrBlank(String sVar)
	{
		return !isNullOrBlank(sVar);
	}//isNotNullOrBlank

	public boolean isNullOrBlank(String sVar)
	{
		boolean result = false;
		if(sVar == null || sVar.isEmpty())
		{
			result = true;
		}
		return result;
	}//isNullOrBlank

	

	public void startApp(WebDriver driver, String sURL){
		driver.get(sURL);
	}

	public String getConfigValue(String skey){
		return Messages.getString(skey);
	}

	public String[] getConfigValues(String sKey,String sDelimiter){
		return Messages.getString(sKey).split(sDelimiter);
	}

	public void sleep(int seconds) 
	{
		for (int i = 0; i < seconds; i++) 
		{
			try 
			{
				Thread.sleep(1000);
			} 
			catch (InterruptedException e) 
			{
				Thread.interrupted();
			}
		}
	}


	public void closeTargetApplication(String processName) throws Exception 
	{   
		Process p = Runtime.getRuntime().exec(TASKLIST);  
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));  
		String line; 
		ArrayList <String> aPID = new ArrayList <String> ();
		boolean flag = false;
		while ((line = reader.readLine()) != null) 
		{    
			//System.out.println(line);
			if (line.contains(processName)) 
			{
				//System.out.println(line);
				{
					flag = true;
					aPID.add(getPID(line));
				}
			}  
		} 

		if(flag)
		{
			for(int i=0;i<aPID.size();i++)
			{
				killProcess(aPID.get(i));
			}
		}
	}  

	private void killProcess(String PID) throws Exception 
	{    
		Runtime.getRuntime().exec(KILL + PID);   
	} 

	private String getPID(String str) throws Exception
	{
		String [] strs = str.split(" ");
		for(int i=1;i<strs.length;i++)
		{
			if(strs[i].length()>2)
			{
				return strs[i];
			}
		}
		throw new Exception("Cannot get PID!");
	}

	public String getClassName(){
		String sClassName = this.getClass().getName();
		String [] saClassNamePath = sClassName.split("\\.");
		return saClassNamePath[saClassNamePath.length-1];
	}

	public String getValueByKey(String sKey){
		return Messages.getString(sKey);
	}

	public String[] readInFileNames(String sPath){
		List<String> listOfLogFiles = new ArrayList<String>();
		File folder = new File(sPath);
		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()&&
					listOfFiles[i].toString().contains(".log")) {
				listOfLogFiles.add(listOfFiles[i].getName());
			}
		}
		return listOfLogFiles.toArray(new String[listOfLogFiles.size()]);
	}
	
	public String[] readInFileNames(String sPath, String sExtension){
		List<String> listOfLogFiles = new ArrayList<String>();
		File folder = new File(sPath);
		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()&&
					listOfFiles[i].toString().contains(sExtension)) {
				listOfLogFiles.add(listOfFiles[i].getName());
			}
		}
		return listOfLogFiles.toArray(new String[listOfLogFiles.size()]);
	}

	public String currentTime(){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd/MM/YYYY");
		return sdf.format(cal.getTime()).toString();
	}

	public void writeToExternalFile(String sFilename, String strToDump) throws IOException{
		File file = new File(sFilename);
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(strToDump);
		} finally {
			if (writer != null) writer.close();
		}
	}

	public String[] readInContentsFromExternalFileAsStringArray(String sPath, String sFilename){
		List<String> listOfContents = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(sPath + sFilename))){
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				listOfContents.add(sCurrentLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listOfContents.toArray(new String[listOfContents.size()]);
	}
	
	public void checkStatusOfPreviousStep(boolean statusOfPreviousStep) throws Exception{
		if(!statusOfPreviousStep){
			throw new Exception("FAIL - Previous Step Failed!");
		}
	}

}
