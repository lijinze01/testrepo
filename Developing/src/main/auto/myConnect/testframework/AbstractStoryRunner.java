package main.auto.myConnect.testframework;

import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;
import static org.jbehave.core.reporters.Format.CONSOLE;
import static org.jbehave.core.reporters.Format.HTML;
import static org.jbehave.core.reporters.Format.XML;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jbehave.core.Embeddable;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.StoryControls;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.RelativePathCalculator;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.StoryReporter;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.jbehave.web.selenium.PerStoriesWebDriverSteps;
import org.jbehave.web.selenium.PropertyWebDriverProvider;
import org.jbehave.web.selenium.SeleniumConfiguration;
import org.jbehave.web.selenium.SeleniumContext;
import org.jbehave.web.selenium.WebDriverProvider;
import org.jbehave.web.selenium.WebDriverScreenshotOnFailure;
import org.jbehave.web.selenium.WebDriverSteps;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.common.util.concurrent.MoreExecutors;


public abstract class AbstractStoryRunner extends JUnitStories 
{
	private static final int STORY_TIMEOUT = 120;

	protected WebDriverProvider driverProvider = new BrowserAgentDriverProvider();
	private WebDriverSteps lifecycleSteps = new PerStoriesWebDriverSteps(driverProvider);
	protected PageFactory factory = new PageFactory(driverProvider);
	private SeleniumContext context = new SeleniumContext();

	public AbstractStoryRunner()
	{
		configuredEmbedder()
		.embedderControls()
		.useStoryTimeoutInSecs(STORY_TIMEOUT)
		.doGenerateViewAfterStories(true)
		.doBatch(true)
		.useThreads(1);

		if (lifecycleSteps instanceof PerStoriesWebDriverSteps) 
		{
			configuredEmbedder().useExecutorService(MoreExecutors.sameThreadExecutor());
		}
	}


	public Configuration configuration() 
	{
		Class<? extends Embeddable> embeddableClass = this.getClass();
		return new SeleniumConfiguration()
		.useSeleniumContext(context)
		.useWebDriverProvider(driverProvider)
		.useStoryLoader(new LoadFromClasspath(embeddableClass))
		.useStoryReporterBuilder(
				new StoryReporterBuilder()
				.withCodeLocation(codeLocationFromClass(embeddableClass))
				.withReporters(testReporters())
				.withDefaultFormats()
				.withFormats(CONSOLE, XML, HTML)
				.withFailureTrace(true).withFailureTraceCompression(true)
				)
				.useStoryControls(new StoryControls().doSkipScenariosAfterFailure(true))
				.usePathCalculator(new RelativePathCalculator());	
	}

	public InjectableStepsFactory stepsFactory() 
	{
		Configuration configuration = configuration();
		List<Object> steps = new ArrayList<Object>();
		steps.addAll(Arrays.asList(getSteps()));
		steps.add(new WebDriverScreenshotOnFailure(driverProvider, configuration.storyReporterBuilder()));
		return new InstanceStepsFactory(configuration, steps.toArray());
	}

	//
	public PageFactory getPages() 
	{
		return factory;
	}


	//
	protected StoryReporter[] testReporters()
	{
		return new StoryReporter[0];
	}

	public static class SameThreadEmbedder extends Embedder
	{
		public SameThreadEmbedder()
		{
			useExecutorService(MoreExecutors.sameThreadExecutor());
		}

	}



	protected String buildStories(String module, String story)
	{
		StringBuffer sb = new StringBuffer();
		sb.append("**")
		.append("/")
		.append(module)
		.append("/story/");

		if (StringUtils.isBlank(story)) 
		{
			sb.append("*");
		} 
		else
		{
			sb.append(story);
		}
		sb.append(".story");

		return sb.toString();
	}


	protected abstract Object[] getSteps();

	public boolean isNullOrBlank(String sVar)
	{
		boolean result = false;
		if(sVar == null || sVar.isEmpty())
		{
			result = true;
		}
		return result;
	}//is




	public class BrowserAgentDriverProvider extends PropertyWebDriverProvider {      
		@Override protected InternetExplorerDriver createInternetExplorerDriver() {        
			DesiredCapabilities dc = DesiredCapabilities.internetExplorer(); 
			dc.setCapability(CapabilityType.PLATFORM, "WINDOWS");
			dc.setCapability(CapabilityType.BROWSER_NAME, "internet explorer");
			dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			dc.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);       
			return new InternetExplorerDriver(dc);     
		}      
	} 

}