package main.auto.myConnect.common.step;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;

import main.auto.myConnect.common.page.AtlassianWelcomePage;
import main.auto.myConnect.common.page.ConfluenceHomePage;
import main.auto.myConnect.testframework.PageFactory;
import main.auto.myConnect.testframework.ScriptSuperClass;


public class LoginToAtlassian extends ScriptSuperClass{

	private AtlassianWelcomePage welcomePage;
	private ConfluenceHomePage homePage;
	
	final Logger logger = LogManager.getLogger(LoginToAtlassian.class);
	
    public LoginToAtlassian(PageFactory factory) 
    {
        this.welcomePage = factory.getPage(AtlassianWelcomePage.class);
        this.homePage = factory.getPage(ConfluenceHomePage.class);
    }
	
    @Given("Launch Atlassian and Login with <url> <username> <password>")
    public void loadURLAndLogin(@Named ("url") String sURL, @Named ("username") String sUsername, @Named ("password") String sPassword) throws Exception 
    {
    	try{
    		// Load application url
        	welcomePage.setPageLoadTimeout(10);
    		welcomePage.load(sURL);
    		logger.info("PASS - Load application url {}", sURL);
        	welcomePage.setPageLoadTimeout(40);
        	
        	// Enter username
        	welcomePage.username().sendKeys(sUsername);
        	sleep(1);
        	// Enter password
        	welcomePage.password().sendKeys(sPassword);
        	sleep(1);
        	
        	// Hit submit button
        	welcomePage.loginBtn().click();
        	
        	// Validate login
        	try{
        		homePage.welcomeText();
        	}catch(Exception e){
        		throw new Exception("Login failed! Cannot find welcome text!");
        	}
        	
        	logger.info("PASS - Logged in as {} password {}", sUsername, sPassword);
        	logger.info("PASS - [" + getClassName() + "]");
    	}catch(Exception e){
    		logger.error("FAIL - loadURLAndLogin");
    		logger.error("Error Description", e);
    		welcomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
    	}
    	
    }
	
	
}
