package main.auto.myConnect.common.step;

import java.util.concurrent.TimeUnit;

import main.auto.myConnect.testframework.BrowserFactory;
import main.auto.myConnect.testframework.ScriptSuperClass;

import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;


public class TestPreparation extends ScriptSuperClass {

	WebDriverProvider driverProvider;

	public TestPreparation(WebDriverProvider driverProvider)
	{
		this.driverProvider = driverProvider;
	}
	
	@BeforeStory
	public void beforeStory() throws Exception
	{
		// Kill existing old webDriver
		closeTargetApplication("chromedriver.exe");// Chrome driver
		closeTargetApplication("IEDriverServer");// IE driver
		
		BrowserFactory.setupBrowser();
		driverProvider.initialize();
		WebDriver driver = driverProvider.get();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		
		driver.manage().window().setSize(new Dimension(1280, 980));
		
		//System.out.println(driver.manage().window().getSize());
		//driver.manage().window().setSize(new Dimension(1280, 980));
		//System.out.println(driver.manage().window().getSize());
		//driver.manage().window().maximize();
		//System.out.println(driver.manage().window().getSize());
	}
	
	
	@AfterStories
	public void afterStories(){
		
		WebDriver driver = driverProvider.get();
		driver.quit();
	}
}
