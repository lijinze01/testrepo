package main.auto.myConnect.common.step;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jbehave.core.annotations.Given;

import main.auto.myConnect.common.page.ConfluenceHomePage;
import main.auto.myConnect.testframework.PageFactory;
import main.auto.myConnect.testframework.ScriptSuperClass;


public class LogoutAtlassian extends ScriptSuperClass{

	private ConfluenceHomePage confluenceHomePage;
	
	final Logger logger = LogManager.getLogger(LogoutAtlassian.class);
	
    public LogoutAtlassian(PageFactory factory) 
    {
        this.confluenceHomePage = factory.getPage(ConfluenceHomePage.class);
    }
	
    @Given("Logout Atlassian")
    public void logoutAtlassian() throws Exception 
    {
    	try{
    		confluenceHomePage.userMenuLink();
    		sleep(1);
    		confluenceHomePage.logoutLink().click();
    		sleep(1);
        	logger.info("PASS - [" + getClassName() + "]");
    	}catch(Exception e){
    		logger.error("FAIL - [" + getClassName() + "]");
    		logger.error("Error Description", e);
    		confluenceHomePage.screenshot("FAIL", getValueByKey("Path.Screenshot"), getClassName());
    	}
    	
    }
	
	
}
