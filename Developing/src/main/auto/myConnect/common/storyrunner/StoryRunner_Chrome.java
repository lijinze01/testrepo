package main.auto.myConnect.common.storyrunner;

import static java.util.Arrays.asList;
import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;

import java.util.List;

import org.jbehave.core.io.StoryFinder;

import main.auto.myConnect.common.step.LoginToAtlassian;
import main.auto.myConnect.common.step.TestPreparation;
import main.auto.myConnect.testframework.AbstractStoryRunner;



public class StoryRunner_Chrome extends AbstractStoryRunner
{
	
	protected Object[] getSteps() 
	{
		System.setProperty("browser", "Chrome");
		return new Object[] { 
				new TestPreparation(driverProvider), 
				new LoginToAtlassian(factory)
		};
	}

	@Override
	protected List<String> storyPaths() {
		String codeLocation = codeLocationFromClass(this.getClass()).getFile();

		String story = System.getProperty("story", "login");

		return new StoryFinder().findPaths(codeLocation,
				asList(buildStories("common", story)), asList(""));
	}
}