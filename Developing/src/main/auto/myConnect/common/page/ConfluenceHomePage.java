package main.auto.myConnect.common.page;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import main.auto.myConnect.testframework.SuperWebDriverPage;

public class ConfluenceHomePage extends SuperWebDriverPage {

	public ConfluenceHomePage(WebDriverProvider driverProvider) {
		super(driverProvider);
	}

	// Welcome text
	public WebElement welcomeText() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("h2[id='WelcometoConfluence']"),3);
	}
	
	// User menu link
	public WebElement userMenuLink() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("a[id='user-menu-link']"),3);
	}
	
	// Logout link
	public WebElement logoutLink() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("a[id='logout-link']"),3);
	}
	
	// Quick create page button
	public WebElement quickCreatePageBtn() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("a[id='quick-create-page-button']"),3);
	}
	
	// Create page button
	public WebElement createPageBtn() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("a[id='create-page-button']"),3);
	}
}
