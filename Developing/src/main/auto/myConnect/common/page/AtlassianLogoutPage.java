package main.auto.myConnect.common.page;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import main.auto.myConnect.testframework.SuperWebDriverPage;

public class AtlassianLogoutPage extends SuperWebDriverPage {

	public AtlassianLogoutPage(WebDriverProvider driverProvider) {
		super(driverProvider);
	}
	
	// Confirm logout heading
	public WebElement confirmLogout() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.xpath("//h2[contains(text(),'Confirm log out')]"), 5);
	}
	
	// Logout button
	public WebElement logoutBtn() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("button[id='logout']"));
	}
	
	// Logged out message
	public WebElement logoutMessage() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("div[id='logged-out']"), 5);
	}
}
