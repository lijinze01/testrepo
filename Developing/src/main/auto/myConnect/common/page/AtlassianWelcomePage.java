package main.auto.myConnect.common.page;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import main.auto.myConnect.testframework.SuperWebDriverPage;

public class AtlassianWelcomePage extends SuperWebDriverPage {

	public AtlassianWelcomePage(WebDriverProvider driverProvider) {
		super(driverProvider);
	}
	
	// Find username input text field
	public WebElement username() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("input[id='username'][name='username']"));
	}
	
	// Find password input text field
	public WebElement password() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("input[id='password'][name='password']"));
	}
	
	// Find login button
	public WebElement loginBtn() throws Exception{
		waitForPageLoad();
		return waitForPresenceOfElement(By.cssSelector("button[id='login'][type='submit']"));
	}
	

}
