package main.auto.myConnect.util.properties;


public class PropertyCollector {
	
	private String sPropertyType;
	private String sPropertyName;
	private Object oPropertyValue;
	
	public String getPropertyName() {
		return sPropertyName;
	}
	public void setPropertyName(String sPropertyName) {
		this.sPropertyName = sPropertyName;
	}
	public Object getPropertyValue() {
		return oPropertyValue;
	}
	public void setPropertyValue(Object oPropertyValue) {
		this.oPropertyValue = oPropertyValue;
	}
	public String getPropertyType() {
		return sPropertyType;
	}
	public void setPropertyType(String sPropertyType) {
		this.sPropertyType = sPropertyType;
	}
	
	
}
