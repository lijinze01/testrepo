package main.auto.myConnect.util;

import org.openqa.selenium.WebDriver;

public interface WebDriverProvider {

	WebDriver get();

    void initialize();

    boolean saveScreenshotTo(String path);

    void end();

}
