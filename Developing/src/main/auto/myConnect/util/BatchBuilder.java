package main.auto.myConnect.util;

import java.io.IOException;

import main.auto.myConnect.testframework.ScriptSuperClass;

public class BatchBuilder extends ScriptSuperClass{

	private static final String workingDir = System.getProperty("user.dir");
	
	public void batchBuilder() throws IOException{
		StringBuilder batchBuilder = new StringBuilder();
		String storyrunnerFolder = workingDir+"\\src\\main\\auto\\myConnect\\regression\\storyrunner";
		String [] listOfStoryRunnerFiles = readInFileNames(storyrunnerFolder, ".java");
		
		batchBuilder.append("cd \""+workingDir+"\"" + "\n");
		for(String storyrunner:listOfStoryRunnerFiles){
			batchBuilder.append("call ant " + storyrunner.split("\\.")[0] + " \"build.xml\"" + "\n");
		}
		batchBuilder.append("call ant LogGenerator" + " \"build.xml\"" + "\n");
		batchBuilder.append("pause");
		
		writeToExternalFile(workingDir + "\\cli\\TestRun.bat", batchBuilder.toString());
	}
	
	public static void main(String[] args) throws IOException {
		BatchBuilder bb = new BatchBuilder();
		bb.batchBuilder();
	}
	
	

}


