package main.auto.myConnect.util;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import main.auto.myConnect.testframework.ScriptSuperClass;

public class LogGenerator extends ScriptSuperClass{

	private static final String workingDir = System.getProperty("user.dir");

	public void reportBuilder() throws IOException, ParseException{
		String sCurrentTime = currentTime();
		StringBuilder reportBuilder = new StringBuilder();
		String[] saLogHeaders = getValueByKey("Log.Header").split(":");
		String sCompanyLogo = workingDir+"\\src\\main\\auto\\myConnect\\util\\bannerlogo.png";
		String sAppLogo = workingDir+"\\src\\main\\auto\\myConnect\\util\\icon-only_primary.png";
		List<Result> listOfResults = resultExtractor();
		int index = 1;
		reportBuilder.append("<html><head><title>Regression Test Summary Results - ["+sCurrentTime+"]</title><link rel=\"stylesheet\" type=\"text/css\" href=\""+workingDir+"\\src\\main\\auto\\myConnect\\util\\report.css\"></head>");
		reportBuilder.append("<body>");
		reportBuilder.append("<div>");
		reportBuilder.append("<img class=\"company-logo\" src=\""+sCompanyLogo+"\" alt=\"logo\" >");
		reportBuilder.append("</div>");
		reportBuilder.append("<div>");
		reportBuilder.append("<img class=\"app-logo\" src=\""+sAppLogo+"\" alt=\"myConnectlogo\" >");
		reportBuilder.append("<h2 >Atlassian</h2>");
		reportBuilder.append("</div>");
		reportBuilder.append("<div>");
		reportBuilder.append("<h3 class=\"test-results-header\" >Regression Test Summary Report - ["+sCurrentTime+"]</h3>");
		reportBuilder.append("</div>");
		reportBuilder.append("<table class=\"test-result-table\" cellspacing=\"0\">");
		reportBuilder.append("<thead>");
		reportBuilder.append("<tr>");
		for(String header:saLogHeaders){
			reportBuilder.append("<td class=\"test-result-table-header-cell\">"+header+"</td>");
		}
		reportBuilder.append("</tr> ");
		reportBuilder.append("</thead>");
		reportBuilder.append("<tbody>");
		for(Result result: listOfResults){
			String sTestCaseName = result.getTestcase();
			String sStatus = result.getStatus();
			String sStartTime = result.getStarttime();
			String sEndTime = result.getEndtime();
			String sDuration = result.getDuration();
			reportBuilder.append("<tr class=\"test-result-step-row test-result-step-row-altone\">");
			reportBuilder.append("<td class=\"test-result-step-command-index\">"+index+"</td>");
			reportBuilder.append("<td class=\"test-result-step-command-cell\" width=\"55%\"><a class=\"testcase\" href=\""+sTestCaseName+"\">"+sTestCaseName+"</a></td>");
			if(sStatus.equalsIgnoreCase("pass")){
				reportBuilder.append("<td class=\"test-result-step-result-cell-ok\">"+sStatus+"</td>");
			}else if(sStatus.equalsIgnoreCase("fail")){
				reportBuilder.append("<td class=\"test-result-step-result-cell-failure\">"+sStatus+"</td>");
			}else{
				reportBuilder.append("<td class=\"test-result-step-command-empty\">"+sStatus+"</td>");
			}
			reportBuilder.append("<td class=\"test-result-step-command-timestamp\" >"+sStartTime+"</td>");
			reportBuilder.append("<td class=\"test-result-step-command-timestamp\" >"+sEndTime+"</td>");
			reportBuilder.append("<td class=\"test-result-step-command-timestamp\" >"+sDuration+"</td>");
			reportBuilder.append("</tr>");
			index ++;
		}
		reportBuilder.append("</tbody>");
		reportBuilder.append("</table>");
		reportBuilder.append("</body>");
		reportBuilder.append("</html>");
		writeToExternalFile(workingDir + "\\logs\\"+getValueByKey("LogName.Html"), reportBuilder.toString());
	}

	public static void main(String[] args) throws NumberFormatException, IOException, ParseException {
		LogGenerator logGenerator = new LogGenerator();
		logGenerator.reportBuilder();
	}

	public List<Result> resultExtractor() throws ParseException{
		List<Result> listOfResult = new ArrayList<Result>();
		String sLogPath = workingDir + "\\logs\\";
		String [] listOfLogFiles = readInFileNames(sLogPath);
		for(String logFile:listOfLogFiles){
			Result result = new Result();
			String sFlag = "";
			String [] saLogContents = readInContentsFromExternalFileAsStringArray(sLogPath, logFile);
			String startTime = "";
			String endTime = "";
			int counter_startTime = 0;
			int counter_endTime = saLogContents.length;
			for (int i = 0; i < saLogContents.length; i++) {
				if(saLogContents[i].contains("FAIL")
						||saLogContents[i].contains("EXCEPTION")
						||saLogContents[i].contains("Exception")
						||saLogContents[i].contains("Fail")
						||saLogContents[i].contains("fail")){
					sFlag = "FAIL";
				}
			}
			
			if(!sFlag.equalsIgnoreCase("FAIL")){
				sFlag = "PASS";
			}

			while(isNullOrBlank(startTime)){
				try{
					startTime = saLogContents[counter_startTime].split(" \\[main\\] ")[0].replace("\n", " ");
					saLogContents[counter_startTime].split(" \\[main\\] ")[1].replace("\n", " ");
				}catch(Exception e){
					startTime = "";
				}
				counter_startTime ++;
				if((counter_startTime == saLogContents.length) || (saLogContents.length == 0)){
					break;
				}
			}

			while(isNullOrBlank(endTime)){
				try{
					endTime = saLogContents[counter_endTime].split(" \\[main\\] ")[0].replace("\n", " ");
					saLogContents[counter_endTime].split(" \\[main\\] ")[1].replace("\n", " ");
				}catch(Exception e){
					endTime = "";
				}
				counter_endTime --;
				if(counter_endTime < 0){
					break;
				}
			}

			result.setStarttime(startTime);
			result.setEndtime(endTime);
			result.setTestcase(logFile);
			if(isNullOrBlank(startTime)&&isNullOrBlank(endTime)){
				sFlag = ""; // If text log is empty
			}
			result.setStatus(sFlag);
			result.setDuration(durationCalculator(startTime,endTime));
			listOfResult.add(result);
		}
		return listOfResult;
	}
	
	public String durationCalculator(String startTime, String endTime) throws ParseException{
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		String duration = "";
		if(isNotNullOrBlank(startTime) && isNotNullOrBlank(endTime)){
			Date dStartTime = format.parse(startTime);
			Date dEndTime = format.parse(endTime);
			long diffInMillies = dEndTime.getTime() - dStartTime.getTime();
			duration = String.format("%d min, %d sec", 
					TimeUnit.MILLISECONDS.toMinutes(diffInMillies),
					TimeUnit.MILLISECONDS.toSeconds(diffInMillies) - 
					TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(diffInMillies))
					);
		}
		return duration;
	}

	// Inner class
	class Result {
		private String testcase = "";
		private String status = "";
		private String starttime = "";
		private String endtime = "";
		private String duration = "";
		public String getTestcase() {
			return testcase;
		}
		public void setTestcase(String testcase) {
			this.testcase = testcase;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getStarttime() {
			return starttime;
		}
		public void setStarttime(String starttime) {
			this.starttime = starttime;
		}
		public String getEndtime() {
			return endtime;
		}
		public void setEndtime(String endtime) {
			this.endtime = endtime;
		}
		public String getDuration() {
			return duration;
		}
		public void setDuration(String duration) {
			this.duration = duration;
		}
	}



}
